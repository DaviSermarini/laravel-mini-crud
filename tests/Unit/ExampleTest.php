<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Backend\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateBanner()
    {
      $this->withoutExceptionHandling();
      $this->actingAs(factory('App\Models\Backend\User')->create());
      $data = [
        'imagem' => $this->faker->sentence,
        'url' => $this->faker->url,
        '_token' => csrf_token()
      ];
      $response = $this->post(route('backend.banners.adicionar'), $data)
        ->assertStatus(201);
      pre($response);
    }
}
