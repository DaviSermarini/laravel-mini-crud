## Laravel Mini CRUD

Este projeto embrionário visa ser uma base para áreas administrativas de sites e sistemas.
Ela utiliza o AdminLTE2 como base, e seu formato SPA utiliza jQuery e Blades.

# Instalação

Clone o projeto em seu diretório escolhido:
> git clone https://DaviSermarini@bitbucket.org/DaviSermarini/laravel-mini-crud.git

Após o clone terminado, acesse o diretório `laravel-mini-crud` pelo terminal e rode os seguintes comandos, na ordem:

- `composer update`
- `cp -a .env.example .env`

Abra o arquivo .env em seu editor/ide preferido, e configure os dados de seu banco de dados. Feito isso, rode mais estes comando, também em ordem:

- `php artisan key:generate`
- `php artisan migrate --seed`
- `php artisan storage:link`
- `php artisan serve`

Com isso, o sistema estará configurado e rodando em (http://127.0.0.1:8000)

O sistema vem com o usuário inicial **admin**, e com a senha **123456**

Inicialmente, você verá 5 menus à esquerda, estes são os exemplos deixados para que você possa estudar todas as possibilidades de campos a serem criados e de configurações possíveis deste sistema.

Os menus estão no arquivo `config/cms.php`

Para criar seus próprios CRUDS, basta criar sua Model, rodar sua migration e configurar os campos a serem exibidos, tanto na listagem quanto nos formulários de inserção e edição.

O projeto é open, sendo assim, qualquer melhoria/correção será muito bem vinda!

Abaixo segue um exemplo de como configurar a listagem a o formulário de uma Model:

```php5
  public $listagem = [
    'imagem' => 'imagemTag',
    'titulo',
    'categoria' => 'categoria_nome',
  ];

  public $formulario = [
    'titulo' => [
      'title' => 'Campo de Texto',
      'type' => 'text',
      'width' => 6,
      'validators' => 'required|string|min:5',
    ],
    'senha' => [
      'title' => 'Campo de Password',
      'type' => 'password',
      'width' => 6,
      'validators' => 'required|string|min:5',
    ],
  ];
```