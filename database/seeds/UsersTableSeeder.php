<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'nome' => 'Administrador',
        'usuario' => 'admin',
        'email' => 'admin@admin.com',
        'senha' => Hash::make('123456'),
        'remember_token' => str_random(60)
    ]);
    }
}
