<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
          $table->unsignedInteger('ativo');
          $table->unsignedInteger('radio');
          $table->unsignedInteger('select');
          $table->unsignedInteger('checkbox');
          $table->string('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
          $table->dropColumn('ativo');
          $table->dropColumn('radio');
          $table->dropColumn('select');
          $table->dropColumn('checkbox');
          $table->dropColumn('password');
        });
    }
}
