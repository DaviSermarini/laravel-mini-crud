<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      setlocale(LC_ALL, 'pt_BR', 'pt_BR.UTF-8', 'pt_BR.utf8', 'portuguese');
      $faker = \Faker\Factory::create('pt_BR');
      require_once app_path('Macros/Functions.php');
      Carbon::setLocale('pt_BR');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
