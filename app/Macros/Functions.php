<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Base\BaseRepository;

function pre($expression, bool $json = true, bool $stop = true)
{
	echo '<pre>';
	if($json)
		echo print_r($expression); 
	else
		var_dump($expression);
	if($stop)
		die();
}

function assets($file = null)
{
	return url('/') . "/resources/assets/{$file}";
}

function dateBdToApp($date){
  $old = new Datetime($date);
  return $old->format('d/m/Y');
}

function dateToApp($date){
  if( $date ){
    $old = new Datetime($date);
    return $old->format('Y-m-d');
  }else{
    return null;
  }
}

function dateToAppYear($date){
  if( $date ){
    $old = new Datetime($date);
    return $old->format('Y');
  }else{
    return null;
  }
}

function dateAppToBd($date){
  if( $date ){
    $old = \Carbon\Carbon::createfromformat('d/m/Y', $date);
    return $old->format('Y-m-d');
  }else{
    return null;
  }
}

function dateAppToBdYear($date){
  if( $date ){
    $old = \Carbon\Carbon::createfromformat('Y', $date);
    return $old->format('Y-m-d');
  }else{
    return null;
  }
}

function dateInFull($date){
  if( $date ){
    $old = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
    return $old->formatLocalized('%e de %B de %Y');
  }else{
    return null;
  }
}

function dateTimeBdToApp($date)
{
  $old = new Datetime($date);
  return $old->format('d/m/Y H:i:s');
}

function currencyToBd($str){
  $s =  preg_replace('/[^0-9,]/','',$str);
  return preg_replace('/[,]/','.',$s);
}

function currencyToApp($curr){
  return 'R$ '.number_format($curr, 2, ',','.');
}

function currencyToAppOnlyNumbers($curr){
  return number_format($curr, 2, ',','.');
}

function break_text($text, $limit) {
  if (strlen($text) > $limit) {
    $pos = strpos($text, ' ', $limit);
    return substr($text, 0, $pos) . '...';
  } else {
    return $text;
  }
}

function limpaNumeros($n){
  return preg_replace("/[^0-9]/", "",$n);
}

function preparaTelefone($n){
  $tel = preg_replace("/[^0-9]/", "",$n);
  $ddd = substr($tel,0,2);
  $numero = substr($tel,2,10);
  return [$ddd,$numero];
}