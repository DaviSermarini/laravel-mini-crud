<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagina extends Model
{

  protected $guarded = ['id'];
  public $type = 'page';

  public $formulario = [
    'banner' => [
      'type' => 'image',
      'title' => 'Banner',
      'width' => 12,
      'validators' => 'nullable',
    ],
    'conteudo' => [
      'type' => 'textarea',
      'title' => 'Conteúdo da página',
      'editor' => true,
      'width' => 12,
      'validators' => 'required|min:20',
    ]
  ];

}
