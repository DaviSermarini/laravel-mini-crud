<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
  protected $guarded = [
    'id',
    'action'
  ];

  protected $appends = [
    'statusLabel'
  ];

  public $listagem = [
    'nome',
    'status' => 'statusLabel'
  ];

  public $formulario = [
    'nome' => [
      'title' => 'Nome da Categoria',
      'type' => 'text',
      'width' => 6,
      'validators' => 'required|min:10|unique:categorias,nome,$this->id',
    ],
  ];

  function ativas(){
    return [
      'filter' => [
        'field' => 'status',
        'operator' => '=',
        'value' => 1,
      ],
      'form' => [
        'status' => [
          'value' => 1,
          'validators' => 'required|int|min:0|max:1',
        ],
      ]
    ];
  }

  function inativas(){
    return [
      'filter' => [
        'field' => 'status',
        'operator' => '=',
        'value' => 0,
      ],
      'form' => [
        'status' => [
          'value' => 0,
          'validators' => 'required|int|min:0|max:1',
        ],
      ]
    ];
  }

  function posts(){
    return $this->hasMany('App\Models\Post');
  }

  function getStatusLabelAttribute(){
    return $this->attributes['statusLabel'] = 'asdasdasd';
  }

}
