<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  
  protected $guarded = [
    'id',
    'tags',
  ];

  protected $appends = [
    'imagemTag',
    'categoria_nome'
  ];

  public $listagem = [
    'imagem' => 'imagemTag',
    'titulo',
    'categoria' => 'categoria_nome',
  ];

  public $formulario = [
    'titulo' => [
      'title' => 'Campo de Texto',
      'type' => 'text',
      'width' => 6,
      'validators' => 'required|string|min:5',
    ],
    'senha' => [
      'title' => 'Campo de Password',
      'type' => 'password',
      'width' => 6,
      'validators' => 'required|string|min:5',
    ],
    'ativo' => [
      'title' => 'Radio via Array',
      'type' => 'radio',
      'src' => 'array',
      'data' => [0 => 'Não', 1 => 'Sim'],
      'width' => 6,
    ],
    'radio' => [
      'title' => 'Radio via BD',
      'type' => 'radio',
      'src' => 'model',
      'data' => 'Categoria',
      'show' => 'nome',
      'width' => 6,
    ],
    'checkbox' => [
      'title' => 'Opções ( checkbox ) via Array',
      'type' => 'checkbox',
      'src' => 'array',
      'data' => [0 => 'Opção 1', 1 => 'Opção 2', 2 => 'Opção 3'],
      'width' => 6,
    ],
    'checkbox2' => [
      'title' => 'Opções ( checkbox ) via BD',
      'type' => 'checkbox',
      'src' => 'model',
      'data' => 'Tag',
      'show' => 'nome',
      'width' => 6,
    ],
    'select' => [
      'title' => 'Select via Array',
      'type' => 'select',
      'src' => 'array',
      'data' => [0 => 'Opção 1', 1 => 'Opção 2', 2 => 'Opção 3'],
      'width' => 6,
    ],
    'select2' => [
      'title' => 'Select via BD',
      'type' => 'select',
      'src' => 'model',
      'data' => 'Categoria',
      'show' => 'nome',
      'width' => 6,
    ],
    'imagem' => [
      'title' => 'Imagem',
      'type' => 'image',
      'width' => 12,
      'validators' => 'required|min:10',
    ],
    'tags' => [
      'title' => 'Belongs To Many',
      'type' => 'manyTo',
      'model' => 'Tag',
      'show' => 'nome',
      'width' => 6,
      'validators' => 'required|exists:tags,id'
    ],
    'categoria_id' => [
      'title' => 'Belongs To',
      'type' => 'belongs',
      'model' => 'Categoria',
      'show' => 'nome',
      'width' => 6
    ],
    'resumo' => [
      'title' => 'Textarea sem editor',
      'type' => 'textarea',
      'width' => 12,
      'editor' => false
    ],
    'conteudo' => [
      'title' => 'Textarea com Editor',
      'type' => 'textarea',
      'width' => 12,
      'editor' => true
    ],
  ];

  function getImagemTagAttribute(){
    return $this->attributes['imagemTag'] = '<img src="'.$this->imagem.'" alt="">';
  }

  function getCategoriaNomeAttribute(){
    return $this->attributes['categoria_nome'] = $this->categoria->nome;
  }

  function tags(){
    return $this->belongsToMany('App\Models\Tag');
  }

  function categoria(){
    return $this->belongsTo('App\Models\Categoria');
  }

}
