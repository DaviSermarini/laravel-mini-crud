<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;
  
	protected $fillable = [
		'nome', 'usuario', 'senha', 'email', 'remember_token',
	];
	protected $hidden = [
		'senha', 'remember_token',
  ];
  
  public $listagem = [
    'nome',
    'Usuário' => 'usuario',
    'email'
  ];

  public $formulario = [
    'nome' => [
      'title' => 'Nome',
      'type' => 'text',
      'width' => 6,
    ],
    'usuario' => [
      'title' => 'Usuário',
      'type' => 'text',
      'width' => 6,
    ],
    'email' => [
      'title' => 'E-mail',
      'type' => 'email',
      'width' => 12,
    ],
    'senha' => [
      'title' => 'Senha',
      'type' => 'password',
      'main' => true,
      'token' => 'remember_token',
      'token_size' => 60,
      'width' => 6,
    ],
    'csenha' => [
      'title' => 'Confirmar Senha',
      'type' => 'password',
      'width' => 6,
    ],
  ];
  
  public function getAuthPassword() {
    return $this->senha;
  }
}