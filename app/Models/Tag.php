<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  
  protected $guarded = ['id'];

  public $listagem = [
    'nome',
  ];

  public $formulario = [
    'nome' => [
      'title' => 'Nome da Tag',
      'type' => 'text',
      'width' => 4,
      'validators' => 'required|min:5|unique:tags,nome,$this->id'
    ],
  ];

  function posts(){
    return $this->belongsToMany('App\Models\Post');
  }


}
