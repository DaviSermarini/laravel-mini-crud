<?php

namespace App\Http\Requests\Backend;

use App\Services\Cms;
use App\Rules\SemImagem;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $values = [];
      if( isset( $this->action ) ){
        $action = $this->action;
        $list = Cms::makeObject($this->model->$action());
        foreach( $list->form as $field => $params ){
          if( isset($params->validators) && $params->validators != null ){
            $values[$field] = str_replace('$this->id', $this->id,$params->validators);
          }
        }
      }
      foreach($this->model->formulario as $field => $params ){
        $f = (object)$params;
        if( isset($f->validators) && $f->validators != null ){
          $values[$field] = str_replace('$this->id', $this->id,$f->validators);
        }
      }
      return $values;
    }

}
