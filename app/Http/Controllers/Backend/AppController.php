<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Base\BaseRepository;
use App\Http\Requests\Backend\BaseRequest;

class AppController extends BackController {

  function index() {
    if (Auth::check()) {
      return response()->redirectTo(route('backend.home'));
    }else{
      return response()->redirectTo(route('backend.form'));
    }
  }

  function modulo(Model $model, String $action = null){
    if( !isset($model->type) || $model->type == 'post' ){
      $view = 'backend.modulo.index';
    }else{
      $view = 'backend.modulo.page';
      $object = BaseRepository::checkObject($model);
    }
    return view($view,[
      'modelName' => class_basename($model),
      'model' => $model,
      'action' => $action,
      'object' => $object ?? '',
    ]);
  }

  function get(Model $model, String $action = null){
    return BaseRepository::get($model, $action);
  }

  function Inserir(Model $model, BaseRequest $request){
    BaseRepository::adicionar($model, $request);
  }

  function Objeto(Model $model, Int $id, String $action = null){
    return BaseRepository::objeto($model, $id, $action);
  }

  function Alterar(Model $model, BaseRequest $request){
    BaseRepository::salvar($model, $request);
  }

  function Apagar(Model $model, Int $id){
    BaseRepository::apagar($model, $id);
    echo 'OK';
  }

}