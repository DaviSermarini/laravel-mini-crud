<?php

namespace App\Http\Controllers\Backend;

use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\BackController;


class HomeController extends BackController {

    function index(Request $request) {
      return view('backend.index');
    }

}