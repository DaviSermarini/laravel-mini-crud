<?php

namespace App\Repositories\Base;

use App\Services\Cms;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Backend\BaseRequest;
use Illuminate\Database\Eloquent\Collection as Col;

class BaseRepository{

  static function get(Model $model, String $action = null){
    if( !$action ){
      $data['data'] = $model::all();
    }else{
      $actions = (object)$model->$action();
      $filter = (object)$actions->filter;
      $data['data'] = $model::where($filter->field, $filter->operator, $filter->value)->get();
    }
    foreach( $data['data'] as $d ){
      $d->acoes = '<button class="btn btn-primary btn-editar-spa btn-sm" data-url="'.route("backend.editar", [class_basename($model),$d->id, $action]).'" data-titulo="Editar '.class_basename($model).'" data-toggle="tooltip" title="Ver / Editar '.class_basename($model).'"><i class="fa fa-pencil-square-o"></i></button>';
      $d->acoes .= ' <button class="btn btn-danger btn-apagar btn-sm" data-url="'.route("backend.apagar", [class_basename($model),$d->id]).'" data-toggle="tooltip" title="Excluir '.class_basename($model).'"><i class="fa fa-trash"></i></button>';
    }
    return response()->json($data);
  }

  static function adicionar(Model $model, BaseRequest $request){
    $request = self::checkPassword($model, $request);
    $object = $model::create($request->all());
    self::checkRelations($object, $request);
  }

  static function objeto(Model $model, Int $id, String $action = null){
    if( is_int($id) ){
      $object = $model::find($id);
      foreach( $model->formulario as $field => $params ){
        $object->action = $action ?? null;
        $f = (object)$params;
        if( $f->type == 'manyTo' ){
          $relation = strtolower($field);
          $load = $object->$relation;
          $object->{"rel_".$relation} = self::toMany($load);
        }
      }
      return $object;
    }
  }

  static function salvar(Model $model, BaseRequest $request){
    $object = $model::find($request->id);
    $request = self::checkPassword($model, $request);
    $object->update($request->all());
    self::checkRelations($object, $request);
  }

  static function checkRelations(Model $model, BaseRequest $request){
    foreach( $model->formulario as $field => $params ){
      $f = (object)$params;
      if( $f->type == 'manyTo' ){
        $attach = strtolower($field);
        $model->$attach()->sync($request->$field);
      }
    }
  }

  static function apagar(Model $model, Int $id){
    if( is_int($id) ){
      $model::find($id)->delete();
    }
  }

  static function toSelect(String $model, String $show){
    $model = self::getModel($model);
    return $model::all()->pluck($show, 'id')->toArray();
  }

  static function toMany(Col $object){
    $out = [];
    foreach($object as $o){
      $out[] = $o->id;
    }
    return $out;
  }

  static function getMany(Model $model, String $attach){
    return $model->$attach;
  }

  static function getModel(String $model){
    $class = 'App\Models\\'.ucfirst($model);
    return new $class();
  }

  static function checkPassword(Model $model, BaseRequest $request){
    $except[] = 'id';
    if( $request->action ){
      $action = $request->action;
      $list = Cms::makeObject($model->$action());
      foreach( $list->form as $field => $params ){
        $except[] = $field;
      }
      $except[] = 'action';
    }
    foreach( $request->except($except) as $k => $v ){
      $field = (object)$model->formulario[$k];
      if( $field->type == 'password' && isset($field->main) ){
        $request[$k] = Hash::make($request->$k);
        $request[$field->token] = str_random($field->token_size);
      }
    }
    return $request;
  }

  static function checkObject(Model $model){
    $record = $model::first();
    if( !$record ){
      $record = new $model();
      $record->save();
    }
    return $record;
  }

}