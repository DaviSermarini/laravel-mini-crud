<?php 

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Base\BaseRepository;

class Cms{

  static function makeMenu(){
    $out = '';
    foreach( config('cms.menu') as $k => $v ){
      $out .= view('backend.shared.links', ['class' => $k, 'params' => (object)$v])->render();
    }
    echo $out;
  }


  static function makeFields(Model $model, String $action = null, Object $object = null){
    $out = '';
    if( $action ){
      $f = self::getForm($model, $action);
      foreach( $f as $k => $v ){
        $out .= view('backend.parts.hidden', ['field' => $k, 'model' => $model, 'params' => $v])->render();
      }
      $v->value = $action;
      $out .= view('backend.parts.hidden', ['field' => 'action', 'model' => $model, 'params' => $v])->render();
    }
    foreach( $model->formulario as $field => $params){
      $f = (object)$params;
      $out .= view('backend.parts.'.$f->type, ['field' => $field, 'model' => $model, 'params' => $f, 'value' => $object->$field ?? null])->render();
    }
    echo $out;
  }

  static function makeTable(Model $model){
    $out = '';
    foreach( $model->listagem as $item => $valor){
      if( is_int($item) ){
        $item = $valor;
      }
      $out .= '<th>'.ucfirst($item).'</th>';
    }
    echo $out;
  }

  static function makeData(Model $model){
    $out = '';
    foreach( $model->listagem as $item => $valor){
      $out .= '{ "data": "'. $valor .'" },';
    }
    echo $out;
  }

  static function getListToSelect(String $model, String $show){
    return BaseRepository::toSelect($model, $show);
  }

  static function getMany(Model $model, String $attach){
    return BaseRepository::getMany($model, $attach);
  }

  static function getForm(Model $model, String $action){
    $params = self::makeObject($model->$action());
    return $params->form;
  }

  static function makeObject($array){
    return json_decode (json_encode ($array), FALSE);
  }


}