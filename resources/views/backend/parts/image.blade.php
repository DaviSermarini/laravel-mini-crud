<div class="form-group col-md-12">
  <label for="nome">{{$params->title}}</label>
  <img class="img-editor imagem fr-fil fr-dib" src="{{$value ?? assets('backend/images/sem-imagem.png')}}" alt="Imagem"/>
  <input type="hidden" name="{{$field}}" value="" class="url-imagem" id="{{$field}}">
</div>