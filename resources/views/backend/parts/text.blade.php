<div class="col-md-{{$params->width}} form-group">
  <label for="nome">{{$params->title}}</label>
  <input type="text" class="form-control" id="{{$field}}" placeholder="{{$params->title}}..." name="{{$field}}" value="{{$value}}">
</div>