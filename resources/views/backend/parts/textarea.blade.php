<div class="col-md-{{$params->width}} form-group">
  <label for="nome">{{$params->title}}</label>
  <textarea class="form-control @if( $params->editor == true ) editor @endif" placeholder="Escreva aqui..." id="{{$field}}" name="{{$field}}">{{$value}}</textarea>
</div>