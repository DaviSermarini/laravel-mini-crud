<div class="col-md-{{$params->width}} form-group">
  <label for="nome">{{$params->title}}</label>
  {!! Form::select($field, [null => 'Selecione uma opção'] + $cms::getListToSelect($params->model, $params->show), null, ['class' => 'form-control select2']) !!}
</div>