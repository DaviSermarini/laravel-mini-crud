@extends('backend.layouts.default')
@section('content')
<section class="content-header">
    <h1>
        Administrativo
        <small>Listagem de {{$modelName}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{route('backend.home')}}"><i class="fa fa-dashboard"></i> Painel Inicial</a></li>
        <li class="active">Listagem de {{$modelName}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Confira abaixo todos os <b>{{$modelName}}(s)</b> cadastrados!</h3>
                    <button class="btn btn-success pull-right btn-novo-spa" data-titulo="Cadastro de Novo {{$modelName}}"><i class="fa fa-plus"></i> Novo {{$modelName}}</button>
                </div>
                <div class="box-body table-responsive">
                    <table class="banners-table table table-responsive table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                  {{$cms::makeTable($model)}}
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>#</th>
                                {{$cms::makeTable($model)}}
                              <th>Ações</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade modal-spa " role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-xl" role="document">
      <div class="box box-info box-solid">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b></b></h4>
              </div>
              <form action="#" role="form" class="form-spa" data-post="{{route('backend.adicionar', $modelName)}}" data-put="{{route('backend.salvar', $modelName)}}">
                <input type="hidden" name="id" value="" class="idobj" id="id">
                <div class="modal-body">
                  <div class="box-body">
                   {{$cms::makeFields($model, $action)}}
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Salvar</button>
                  </div>
                </div>
              </form>
          </div><!-- /.modal-content -->
      </div>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  $(document).ready(function(){
    tabela = $('.table').not('.normal').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.18/i18n/Portuguese-Brasil.json"
      },
      "ajax": {
        "url": "{{route('backend.get', [$modelName, $action ?? ''])}}",
        "type": "GET"
      },
      "columns": [
        { "data": "id" },
        {{$cms::makeData($model)}}
        { "data": "acoes" },
      ],
      "order": [
        [0, "asc"]
      ],
      'paging': true,
      "pageLength": 100,
      'lengthChange': true,
      'searching': true,
      'ordering': true,
      'info': true,
      'autoWidth': false,
      dom: 'lBfrtip',
      buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });
    $( document ).ajaxComplete(function() {
      $('[data-toggle="tooltip"]').tooltip();
    })
    
  })
</script>
@endsection