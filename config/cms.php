<?php

  return [
    'menu' => [
      'user' => [
        'title' => 'Usuários',
        'icon' => 'fa-user'
      ],
      'categoria' => [
        'top' => true,
        'title' => 'Categorias',
        'icon' => 'fa-list',
        'subs' => [
          'ativas' => [
            'title' => 'Ativas',
            'icon' => 'fa-check',
          ],
          'inativas' => [
            'title' => 'Inativas',
            'icon' => 'fa-ban',
          ],
        ]
      ],
      'tag' => [
        'title' => 'Tags',
        'icon' => 'fa-tags'
      ],
      'post' => [
        'title' => 'Posts',
        'icon' => 'fa-file-text'
      ],
      'pagina' => [
        'title' => 'Página ',
        'icon' => 'fa-file'
      ],
    ],
  ];