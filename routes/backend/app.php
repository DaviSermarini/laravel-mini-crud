<?php
Route::group([ 
  'prefix' => '/backend'
], function()
{

  Route::get('/', [
    'as' => 'backend.index',
    'uses' => 'Backend\AppController@index'
    ]);
    
  Route::get('/login', [
    'as' => 'backend.form',
    'uses' => 'Backend\LoginController@Form'
  ]);
      
  Route::post('/login', [
    'as' => 'backend.login',
    'uses' => 'Backend\LoginController@Index'
  ]);
    
});

Route::group([ 
  'middleware' => 'auth',
  'prefix' => '/backend'
], function()
{
  Route::get('/home', [
    'as' => 'backend.home',
    'uses' => 'Backend\HomeController@index'
  ]);
    
  Route::get('/sair', [
    'as' => 'sair',
    'uses' => 'Backend\LoginController@Sair'
  ]);

  Route::post('/upload', [
    'as' => 'backend.ajax.upload',
    'uses' => 'Backend\ImageController@Upload'
  ]);

  Route::post('/load', [
    'as' => 'backend.ajax.load',
    'uses' => 'Backend\ImageController@Load'
  ]);
      
  Route::delete('/delete', [
    'as' => 'backend.ajax.delete',
    'uses' => 'Backend\ImageController@Delete'
  ]);

  Route::get('/modulo/{model}/{acao?}', [
    'as' => 'backend.model',
    'uses' => 'Backend\AppController@Modulo'
  ]);
      
  Route::post('/{model}', [
    'as' => 'backend.adicionar',
    'uses' => 'Backend\AppController@Inserir'
  ]);
      
  Route::put('/{model}', [
    'as' => 'backend.salvar',
    'uses' => 'Backend\AppController@Alterar'
  ]);
      
  Route::get('/get/{model}/{action?}', [
    'as' => 'backend.get',
    'uses' => 'Backend\AppController@Get'
  ]);

  Route::get('/{model}/{id}/{action?}', [
    'as' => 'backend.editar',
    'uses' => 'Backend\AppController@Objeto'
  ]);
      
  Route::delete('/{model}/{id}', [
    'as' => 'backend.apagar',
    'uses' => 'Backend\AppController@Apagar'
  ]);
      
});
