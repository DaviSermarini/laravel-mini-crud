const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .copy('resources/imgs', 'public/imgs')
    // .copy('resources/images', 'public/images')
    // .copy('resources/css', 'public/css')
    // .copy('resources/js', 'public/js');
